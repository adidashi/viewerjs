import _Viewer from './js/viewer';
import _Preview from './js/preview/index';

export const Preview = _Preview;
export const Viewer = _Viewer;

export default _Viewer;
